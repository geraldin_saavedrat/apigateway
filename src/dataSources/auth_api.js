const { RESTDataSource } = require('apollo-datasource-rest');
const serverConfig = require('../server');
class AuthAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = serverConfig.auth_api_url;
}
    async createUser(user) {
        user = new Object(JSON.parse(JSON.stringify(user)));
        return await this.post(`/createUser/`, user);
}
    async getUser(userId) {
        return await this.get(`/detailUser/${userId}/`);
    }
    async authRequest(credentials) {
        credentials = new Object(JSON.parse(JSON.stringify(credentials)));
        return await this.post(`/login/`, credentials);
    }
    async refreshToken(token) {
        token = new Object(JSON.parse(JSON.stringify({ refresh: token })));
        return await this.post(`/refresh/`, token);
    }
    async deleteUser(user) {
        user = new Object(JSON.parse(JSON.stringify(user)));

        return await this.post(`/deleteUser/${user.get(userName)}/${user.get(id)}/`);
}
    async updateUser(user) {
        user = new Object(JSON.parse(JSON.stringify(user)));
        return await this.post(`/updateUser/`, user);
}
    async createPreference(preference) {
    preference = new Object(JSON.parse(JSON.stringify(preference)));
    return await this.post(`/User/createPreference/`, preference);
}
    async getPreference(userId) {
        return await this.get(`/User/detailPreference/${userId}/`); 
}
    async deletePreference(preferenceID) {
        
        return await this.post(`User/deletePreference/${preferenceID}`);
    }
    async updatePreference(preference) {
        preference = new Object(JSON.parse(JSON.stringify(preference)));
    return await this.post(`User/updatePreference/`, preference);
    }
}
module.exports = AuthAPI;