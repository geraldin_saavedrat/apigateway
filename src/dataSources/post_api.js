const { RESTDataSource } = require('apollo-datasource-rest');
const serverConfig = require('../server');
class AccountAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = serverConfig.account_api_url;
    }
    async createComments(comments) {
        comments = new Object(JSON.parse(JSON.stringify(account)));
        return await this.post('/comments', comments);
    }
    async commentsByUsername(username) {
        return await this.get(`/comments/${username}`);
    }
    async createInterest(interest) {
        transaction = new Object(JSON.parse(JSON.stringify(interest)));
        return await this.post('/interest', interest);
    }
    async interestByUsername(username) {
        return await this.get(`/interest/${username}`);
    }
    async createPost(post) {
        post = new Object(JSON.parse(JSON.stringify(post)));
        return await this.post('/post', interest);
    }
    async postByUsername(username) {
        return await this.get(`/post/${username}`);
    }
    }
 module.exports = AccountAPI;