const commentsResolver = {
    Query: {
    commentsByUsername: async(_, { username }, { dataSources, userIdToken }) => {
    usernameToken = (await dataSources.authAPI.getUser(userIdToken)).username
    if (username == usernameToken)
    return await dataSources.postAPI.commentsByUsername(username)
    else
    return null
    
    },
    },
   
Mutation: {
    createComment: async(_, { commentI }, { dataSources, userIdToken }) => {
    usernameToken = (await dataSources.authAPI.getUser(userIdToken)).username
    if (commentI.username == usernameToken)
    return dataSources.postAPI.createComments(commentI)
    else
    return null
    
    }
    }
};
module.exports = commentsResolver;