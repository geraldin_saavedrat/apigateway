const commentsResolver = require('./comments_resolver');
const interestResolver = require('./interest_resolver');
const authResolver = require('./auth_resolver');
const postResolver = require('./post_resolver');
const lodash = require('lodash');
const resolvers = lodash.merge(commentsResolver, interestResolver, authResolver,
    postResolver);
module.exports = resolvers;