//Se llama al typedef (esquema) de cada submodulo
const commentsTypeDefs = require('./comments_type_defs');
const interestTypeDefs = require('./interest_type_defs');
const authTypeDefs = require('./auth_type_defs');
const postTypeDefs = require('./post_type_defs');
const preferencesTypeDefs = require('./preferences_type_defs');
//Se unen
const schemasArrays = [authTypeDefs, commentsTypeDefs, interestTypeDefs,
postTypeDefs, preferencesTypeDefs];
//Se exportan
module.exports = schemasArrays; 