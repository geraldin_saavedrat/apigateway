const { gql } = require('apollo-server');
const interestTypeDefs = gql `
type interest {
id: String!
idUserFK: String!
idPostFK: String!
interestDate: String!
userName: String!
}

extend type Query {
interestByUsername(userName: String!): interest
}
`;
module.exports = interestTypeDefs;