 const { gql } = require('apollo-server');
const preferencesTypeDefs = gql `
type Preferences {
id: String!
idUser: String!
CategoryName: String!
}
input PreferencesInput{
    idUser: String!
    CategoryName: String!
    }
input PreferencesInputU {
    idUser: String!
    CategoryName: String!
    }
    
    input PreferencesInputD {
        id: String!
        }
extend type Query {
preferenceDetailById(id: String!):Preferences
}
extend type Mutation {
createPreference(preferenceInput : PreferencesInput): Tokens!
updatePreference(preferencesI: PreferencesInputU!): Preferences!
deletePreference(preferencesD: PreferencesInputD!): Preferences!
}
`;
module.exports = preferencesTypeDefs;