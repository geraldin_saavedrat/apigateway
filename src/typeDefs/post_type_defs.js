const { gql } = require('apollo-server');
const postTypeDefs = gql `
type post {
    id: String!
    titlePost: String!
    descriptionPost: String!
    imagePost: String!
    datePost: String!
    idUserFK: String!
    userName: String!
    nameCategory: String!
}
input postInput{
    titlePost: String!
    descriptionPost: String!
    imagePost: String!
    datePost: String!
    idUserFK: String!
    userName: String!
    nameCategory: String!
}
extend type Query {
postByUsername(userName: String!): post
}
extend type Mutation{
    createPost(postI : postInput!): post
}
`;
module.exports = postTypeDefs;